/**
 * Automatically generated file. DO NOT MODIFY
 */
package ngaoschos.order_app;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "ngaoschos.order_app";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 8;
  public static final String VERSION_NAME = "1.0.7";
}
